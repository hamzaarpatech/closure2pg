import UIKit

func filterGreaterValue(value:Int , numbers:[Int]) -> [Int] { //numbers:[Int] = array of int numbers
    var filteredNumbers = [Int]() //initiliazing empty array
    for num in numbers {
        if num < value {
            filteredNumbers.append(num)
        }
    }
    return filteredNumbers
}
let filteredList = filterGreaterValue(value: 10, numbers: [1,2,3,5,10,15])
print(filteredList)

// Same functionality with closure logic -----------------------------------------------------------

func filterValueWithClosure(closure : (Int) -> Bool , numbers : [Int]) -> [Int] {
    var filterLessNumbers = [Int]()
    for num in numbers {
        if closure(num){
            filterLessNumbers.append(num)
      }
    }
  return filterLessNumbers
}

let filterClosure = filterValueWithClosure(closure: { (num) -> Bool in
    return num < 20
}, numbers: [1,2,3,5,10,15,20])
print(filterClosure)

// simplifying more code in closures ----------------------------------------------------------------

func ExtractAlphabets(closure : (Character) -> Bool , chars : [Character]) -> [Character] {
    var filteredChars = [Character]()
    for val in chars {
        if closure(val){
            filteredChars.append(val)
        }
    }
    return filteredChars
}

func simplifyCondition(values : Character) -> Bool {
   return values < "y"
}
//updated method
let result = ExtractAlphabets(closure: simplifyCondition(values: ), chars: ["a","b","c","d","x","y","z"])
print(result)
//old methodology
let MineCharacters  = ExtractAlphabets(closure: { (val) -> Bool in
    return val < "y"
}, chars: ["a","b","c","d","x","y","z"])
print(MineCharacters)



class A{
    func test(){
        let b = B()
        b.closure = { str in
            print("val : \(str)")
        }
    }
}

class B {
    var closure : ((String) -> Void)?
    func name(){
        closure?("hamza")
    }
}

let a = A()
a.test()




