//
//  FirstVC.swift
//  Closure2pg
//
//  Created by Hamza on 2/12/20.
//  Copyright © 2020 Hamza. All rights reserved.
//

import UIKit

class FirstVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(textlabel)
        view.addSubview(showButton)
        updateConstraints()
        self.view.backgroundColor = .cyan
    }
    
    private let textlabel : UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .black
        label.backgroundColor = .green
        label.textAlignment = .center
        label.numberOfLines = 3
        label.text = "my text"
        return label
    }()
    
    private let showButton : UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("MoveVc", for: .normal)
        button.tintColor = .white
        button.layer.cornerRadius = 9.0
        button.setTitleColor(.white, for: .normal)
        button.backgroundColor = .black
        button.addTarget(self, action: #selector(showSecondViewController(sender:)), for: .touchUpInside)
        return button
    }()
   
    private func updateConstraints(){
        textlabel.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 100).isActive = true
        textlabel.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -8).isActive = true
        textlabel.leadingAnchor.constraint(equalTo: self.view.leadingAnchor , constant: 8).isActive = true
        textlabel.heightAnchor.constraint(equalToConstant: 40 ).isActive = true

        showButton.topAnchor.constraint(equalTo: textlabel.bottomAnchor , constant: 20).isActive = true
        showButton.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        showButton.heightAnchor.constraint(equalToConstant: 70).isActive = true
        showButton.widthAnchor.constraint(equalToConstant: 100).isActive = true
        
        self.view.setNeedsLayout()
    }
    
    @objc private func showSecondViewController(sender : UIButton){
        if let vc = self.storyboard?.instantiateViewController(identifier: "SecondVC") as?  SecondVC{
            vc.buttonAction = { [weak self] (text) -> () in
              self?.textlabel.text = text
              return vc.dismiss(animated: true, completion: nil)
            }
            present(vc, animated: true, completion: nil)
        }
        
    }
    


}
