//
//  SecondVC.swift
//  Closure2pg
//
//  Created by Hamza on 2/12/20.
//  Copyright © 2020 Hamza. All rights reserved.
//

import UIKit

class SecondVC: UIViewController {
    
    public var buttonAction: ((String) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(textField)
        view.addSubview(cancelButton)
        view.addSubview(updateButton)
        addConstraints()
    }
    
    private lazy var textField: UITextField = {
      let txtfield = UITextField()
        txtfield.translatesAutoresizingMaskIntoConstraints = false
        txtfield.backgroundColor = .green
        txtfield.layer.cornerRadius = 9.0
        txtfield.tintColor = .black
        txtfield.returnKeyType = .emergencyCall
      return txtfield
    }()
    
    private lazy var cancelButton : UIButton = {
        let cancelBt = UIButton()
        cancelBt.translatesAutoresizingMaskIntoConstraints = false
        cancelBt.setTitle("Cancel", for: .normal)
        cancelBt.setTitleColor(.black, for: .normal)
        cancelBt.layer.cornerRadius = 9.0
        cancelBt.backgroundColor = .gray
        cancelBt.addTarget(self, action: #selector(cancel), for: .touchUpInside)
        return cancelBt
    }()
    
    private lazy var updateButton : UIButton = {
       let updateBt = UIButton()
        updateBt.translatesAutoresizingMaskIntoConstraints = false
        updateBt.setTitle("Update", for: .normal)
        updateBt.setTitleColor(.black, for: .normal)
        updateBt.layer.cornerRadius = 9.0
        updateBt.backgroundColor = .gray
        updateBt.addTarget(self, action: #selector(update), for: .touchUpInside)
        return updateBt
    }()
    
    @objc func cancel(){
        dismiss(animated: true, completion: nil)
    }

    @objc func update(){
       let val = textField.text!
        if val != nil{
           guard let buttonAction = buttonAction else {
               return dismiss(animated: true, completion: nil)
           }
           buttonAction(val)
        }
        else{
            print("haha")
        }
    }
    
    func addConstraints(){
    NSLayoutConstraint.activate([
               textField.topAnchor.constraint(equalTo: self.view.topAnchor ,constant: 100),
               textField.leadingAnchor.constraint(equalTo: self.view.leadingAnchor , constant: 20),
               textField.trailingAnchor.constraint(equalTo: self.view.trailingAnchor , constant: -20),
               textField.heightAnchor.constraint(equalToConstant: 30),
               
               cancelButton.heightAnchor.constraint(equalToConstant: 40),
               cancelButton.widthAnchor.constraint(equalToConstant: 70),
               cancelButton.topAnchor.constraint(equalTo: textField.bottomAnchor , constant: 30 ),
               cancelButton.leadingAnchor.constraint(equalTo: self.view.leadingAnchor , constant: 30),
               
               updateButton.heightAnchor.constraint(equalToConstant : 40),
               updateButton.widthAnchor.constraint(equalToConstant: 70),
               updateButton.topAnchor.constraint(equalTo: textField.bottomAnchor , constant: 30),
               updateButton.trailingAnchor.constraint(equalTo: self.view.trailingAnchor , constant: -30),
        ])
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        textField.becomeFirstResponder()
    }

}
